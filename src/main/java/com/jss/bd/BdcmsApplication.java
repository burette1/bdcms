package com.jss.bd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BdcmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BdcmsApplication.class, args);
	}

}

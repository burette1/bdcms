package com.jss.bd.controller;

import com.jss.bd.entity.JotterArticle;
import com.jss.bd.result.Result;
import com.jss.bd.result.ResultFactory;
import com.jss.bd.service.JotterArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
public class JotterArticleController {

    @Autowired
    JotterArticleService jotterArticleService;

    /**
     * 保存公告或者文章
     *
     * @param article
     * @return
     */
    @PostMapping("/api/admin/article")
    public Result saveArticle(@RequestBody JotterArticle article) {
        jotterArticleService.addOrUpdate(article);
        return ResultFactory.buildSuccessResult("保存成功");
    }

    /**
     * 分页功能
     *
     * @param size
     * @param page
     * @return
     */
    @GetMapping("/api/admin/article/{size}/{page}")
    public Result listArticles(@PathVariable("size") int size, @PathVariable("page") int page) {
        return ResultFactory.buildSuccessResult(jotterArticleService.list(page - 1, size));
    }

    /**
     * 查看指定ID的文章
     *
     * @param id
     * @return
     */
    @GetMapping("/api/admin/article/{id}")
    public Result getOneArticle(@PathVariable("id") int id) {
        return ResultFactory.buildSuccessResult(jotterArticleService.findById(id));
    }

    /**
     * 删除文章或者公告
     *
     * @param id
     * @return
     */
    @DeleteMapping("/api/admin/article/{id}")
    public Result deleteArticle(@PathVariable("id") int id) {
        jotterArticleService.delete(id);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @GetMapping("/api/admin/article/deletes")
    public Result deleteBatch(@PathParam("ids") String ids) {
        String[] stuList = ids.split(",");
        // 将字符串数组转为List<Intger> 类型
        List<Integer> LString = new ArrayList<Integer>();
        for (String str : stuList) {
            LString.add(new Integer(str));
        }
        // 调用service层的批量删除函数
        jotterArticleService.deletes(LString);
        return ResultFactory.buildSuccessResult("批量删除成功");
    }
}
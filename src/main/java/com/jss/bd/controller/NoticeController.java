package com.jss.bd.controller;

import com.jss.bd.entity.Notice;
import com.jss.bd.result.Result;
import com.jss.bd.result.ResultFactory;
import com.jss.bd.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
public class NoticeController {
    @Autowired
    NoticeService noticeService;

    /**
     * 保存公告
     *
     * @param notice
     * @return
     */
    @PostMapping("api/admin/notice")
    public Result saveArticle(@RequestBody Notice notice) {
        System.out.println(notice.toString());
        noticeService.addOrUpdate(notice);
        return ResultFactory.buildSuccessResult("保存成功");
    }

    /**
     * 分页功能
     *
     * @param size
     * @param page
     * @return
     */
    @GetMapping("/api/admin/notice/{size}/{page}")
    public Result listNotices(@PathVariable("size") int size, @PathVariable("page") int page) {
        return ResultFactory.buildSuccessResult(noticeService.list(page - 1, size));
    }

    /**
     * 查看指定ID的公告
     *
     * @param id
     * @return
     */
    @GetMapping("/api/admin/notice/{id}")
    public Result getOneNotice(@PathVariable("id") int id) {
        return ResultFactory.buildSuccessResult(noticeService.findById(id));
    }

    /**
     * 删除新闻
     *
     * @param id
     * @return
     */
    @DeleteMapping("/api/admin/notice/{id}")
    public Result deleteNotice(@PathVariable("id") int id) {
        noticeService.delete(id);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @GetMapping("api/admin/notice/deletes")
    public Result deleteBatch(@PathParam("ids") String ids) {
        String[] stuList = ids.split(",");
        // 将字符串数组转为List<Intger> 类型
        List<Integer> LString = new ArrayList<Integer>();
        for (String str : stuList) {
            LString.add(new Integer(str));
        }
        // 调用service层的批量删除函数
        noticeService.deletes(LString);
        return ResultFactory.buildSuccessResult("批量删除成功");
    }
}

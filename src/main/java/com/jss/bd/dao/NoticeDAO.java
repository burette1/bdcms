package com.jss.bd.dao;

import com.jss.bd.entity.Notice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface NoticeDAO extends JpaRepository<Notice, Integer> {

    Notice findById(int id);

    @Modifying
    @Transactional
    @Query("delete from Notice s where s.id in (?1)")
    void deleteBatch(List<Integer> ids);
}

package com.jss.bd.dao;

import com.jss.bd.entity.Swiper;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SwiperDAO extends JpaRepository<Swiper, Integer> {

    Swiper findById(int id);

}

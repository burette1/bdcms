package com.jss.bd.entity;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@Table(name = "Notice")
public class Notice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    /**
     * notice title.
     */
    private String noticeTitle;

    /**
     * notice content after render to html.
     */
    @Lob
    @Column(columnDefinition="text")
    private String noticeContentHtml;

    /**
     * notice content in markdown syntax.
     */
    private String noticeContentMd;

    /**
     * notice abstract.
     */
    private String noticeAbstract;

    /**
     * notice cover's url.
     */
    private String noticeCover;

    /**
     * notice release date.
     */
    private String noticeDate;

    private Date createTime;

    private Date updateTime;
}

package com.jss.bd.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * 轮播图
 */
@Data
@Entity
@Table(name = "swiper")
public class Swiper {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    // 轮播图标题
    private String title;

    // 轮播图备注
    private String remark;

    // 轮播图图片链接
    private String url;

    private Date createTime;

    private Date updateTime;
}

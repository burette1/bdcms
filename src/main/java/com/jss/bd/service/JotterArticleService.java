package com.jss.bd.service;

import com.jss.bd.dao.JotterArticleDAO;
import com.jss.bd.entity.JotterArticle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JotterArticleService {

    @Autowired
    JotterArticleDAO jotterArticleDAO;

    /**
     * 获取对于页数的内容
     *
     * @param page 页码
     * @param size 数量
     * @return 对应页码的内容
     */
    public Page list(int page, int size) {
        return jotterArticleDAO.findAll(PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "id")));
    }

    /**
     * 用ID获取指定内容
     *
     * @param id
     * @return
     */
    public JotterArticle findById(int id) {
        return jotterArticleDAO.findById(id);
    }

    /**
     * 新增或者修改内容
     *
     * @param jotterArticle
     */
    public void addOrUpdate(JotterArticle jotterArticle) {
        jotterArticleDAO.save(jotterArticle);
    }

    /**
     * 删除
     *
     * @param id
     */
    public void delete(int id) {
        jotterArticleDAO.deleteById(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    public void deletes(List<Integer> ids) {
        jotterArticleDAO.deleteBatch(ids);
    }
}

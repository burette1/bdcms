package com.jss.bd.service;

import com.jss.bd.dao.NoticeDAO;
import com.jss.bd.entity.Notice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoticeService {

    @Autowired
    NoticeDAO noticeDAO;

    /**
     * 获取对于页数的内容
     *
     * @param page 页码
     * @param size 数量
     * @return 对应页码的内容
     */
    public Page list(int page, int size) {
        return noticeDAO.findAll(PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "id")));
    }

    /**
     * 用ID获取指定内容
     *
     * @param id
     * @return
     */
    public Notice findById(int id) {
        return noticeDAO.findById(id);
    }

    /**
     * 新增或者修改内容
     *
     * @param news
     */
    public void addOrUpdate(Notice news) {
        noticeDAO.save(news);
    }

    /**
     * 删除
     *
     * @param id
     */
    public void delete(int id) {
        noticeDAO.deleteById(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    public void deletes(List<Integer> ids) {
        noticeDAO.deleteBatch(ids);
    }
}

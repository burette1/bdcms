package com.jss.bd.service;

import com.jss.bd.dao.SwiperDAO;
import com.jss.bd.entity.Swiper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class SwiperService {

    @Autowired
    SwiperDAO swiperDAO;

    /**
     * @param page
     * @param size
     * @return
     */
    public Page list(int page, int size) {
        return swiperDAO.findAll(PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "id")));
    }

    /**
     *
     * @param id
     * @return
     */
    public Swiper findById(int id) {
        return swiperDAO.findById(id);
    }

    /**
     *
     * @param swiper
     */
    public void addOrUpdate(Swiper swiper) {
        swiperDAO.save(swiper);
    }

    /**
     *
     * @param id
     */
    public void delete(int id) {
        swiperDAO.deleteById(id);
    }
}
